% Created 2019-11-26 Tue 21:30
% Intended LaTeX compiler: pdflatex
\documentclass[a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\input{headers-teachers}
\lhead{org-teaching}
\rhead{Example lesson}
\lfoot{TEACHERS HANDBOOK}
\newenvironment{NOTES}{\begin{lrbox}{\mybox}\begin{minipage}{0.9\textwidth}\begin{shaded}}{\end{shaded}\end{minipage}\end{lrbox}\fbox{\usebox{\mybox}}}
\author{Olivier Berger}
\date{2019-07-08}
\title{How to teach using org-mode for fun and profit}
\hypersetup{
 pdfauthor={Olivier Berger},
 pdftitle={How to teach using org-mode for fun and profit},
 pdfkeywords={},
 pdfsubject={Olivier Berger's org-mode framework for teaching},
 pdfcreator={Emacs 26.1 (Org mode 9.2.4)}, 
 pdflang={English}}
\begin{document}

\maketitle
\setcounter{tocdepth}{2}
\tableofcontents

\begin{ABSTRACT}
\begin{shaded}
This document is meant to be read only by the teachers
\end{shaded}
\end{ABSTRACT}

\section{Introduction}
\label{sec:org9c0ee5a}

This is a demo document about the
\href{http://www-public.tem-tsp.eu/\~berger\_o/org-teaching/}{codename \texttt{org-teaching}} 
framework, which aims at managing teaching material using Org-mode.

\section{Org-mode powa}
\label{sec:org3ce69d6}

Attention, this framework heavily relies on: 

\begin{itemize}
\item \href{http://orgmode.org/}{org-mode} (version 9 at the time of writing)
\item and the \href{https://github.com/yjwen/org-reveal/}{org-reveal} exporter for \texttt{reveal.js}.
\end{itemize}

\begin{NOTES}
Since Org-mode is plain text, you may be able to edit contents without
Emacs and org-mode, but I'd recommend learning org-mode for serious
work ;-)
\end{NOTES}

\section{About this PDF handbook}
\label{sec:orgbb6fc68}

This PDF handbook is one variant of the same teaching material, also
available \href{./slides.html}{as a slides deck}. Note that the layout is
formatted as a portrait handbook, including the content placed in the
speaker notes of slides\footnote{the greyed box in the previous section is an example.}.

This section may contain content that is best viewed in (\LaTeX{}) PDF export of org-mode.

\subsection{License}
\label{sec:orgaed82af}

The contents of this project is Copyright (c) 2016-2018 Olivier
Berger - IMT/Télécom SudParis, unless otherwise specified. See the
\url{LICENSE} file for more details.

\section{Features}
\label{sec:org7fd7e37}
\subsection{Writing teaching material in org-mode}
\label{sec:org8cbcf83}

The goal is to be able to edit a single file (namely \texttt{lesson.org})
which will contain, in a \textbf{single source}, all the content of a lesson,
written with org-mode syntax.

From this single source, several documents are generated :
\begin{itemize}
\item \textbf{slides} (as a dynamic Web document) for overhead presentation
\item a \textbf{handbook} that contains the same information (or more) and can be
handed to the \uline{students} for work outside the classroom (a \href{handbook.pdf}{PDF file})
\end{itemize}

\begin{itemize}
\item optionaly, another version of the \textbf{handbook} for the \uline{teaching team}, to provide additional instructions (also a \href{teacher-handbook.pdf}{PDF file})
\end{itemize}

\begin{NOTES}
The student handbook's \LaTeX{} formatting has a summarized content table and nice looking (in principle) title page. Customize at will.

The teachers handbook contains a less nice-looking format, includes a detailed table of contents, and has a watermark. Again, customize at will and submit improvements.
\end{NOTES}

\subsection{Frugal org-reveal/reveal.js slides}
\label{sec:orgfbb7292}

Pretty much all features of \texttt{reveal.js}, supported by the org-mode reveal.js exporter (\href{https://github.com/yjwen/org-reveal/}{org-reveal}), should be supported too.

If you're already familiar with reveal.js, you may have noticed that
the current settings adopted for our slides generation are quite
frugal: no fancy 3D effects and likes.


\begin{NOTES}
It's a matter of taste : I didn't want to show off, and prefer to give
students a clear content on the projector behind me.

An example \texttt{org-reveal} document is \href{elisp/org-reveal/Readme.html}{available here} for inspiration (it's the export of org-reveal's \texttt{README.org}, actually).
\end{NOTES}

\subsection{Structure of the sections / slides}
\label{sec:org623adcf}

I'm using the 3 levels of outlining / sectioning so that the content can be sectioned in the same way in \texttt{lesson.org} and appear appropriately in the slides and handbook, with these principles:

\begin{enumerate}
\item First level outlines define main sections of the document.
\item Second level outlines are the main "horizontal" slides that will be played with page up/down
\item Third level outlines may be used for additional content ("vertical" slides) that may be skipped for the presentation, but is still accessible with cursor keys.
\end{enumerate}

\begin{NOTES}
The first level outlines can be rendered as a "separating" slides which may get a different \texttt{reveal\_background} and \texttt{class="center"} slide layout, but that isn't automatic. See \hyperref[sec:orge1f0584]{Section separators}.
\end{NOTES}

\subsection{Presenter notes / content for the handbook}
\label{sec:org9f4eb94}

\href{https://github.com/yjwen/org-reveal/\#speaker-notes}{org-reveal's \emph{Speaker notes}} may be added to the slides (and will only appear on
dual-screen presentation after having pressed '\emph{s}': standard reveal.js
feature).

They will be masked for the audience, but will, by default, appear in the handbook given to the students.

The syntax in the org-mode source is:
\begin{verbatim}
#+BEGIN_NOTES
This is a note
#+END_NOTES
\end{verbatim}

\begin{NOTES}
This is a note
\end{NOTES}

\subsubsection{Usage}
\label{sec:org9255e19}

I've adopted this principle of exporting everything in the speaker
notes to the students handbook, but YMMV. I'm not even sure this makes
a lot of sense on a pedagogical ground.

In case you're not completely satisfied, this could be modified by
hacking the way the \LaTeX{} PDF export works.

An alternative is to use maked sections (see next section).

\subsection{Masking content for some audiences}
\label{sec:org9fd7f0f}

I've implemented some "easy ways" to preserve some of the content of the same \texttt{lesson.org} source for certain outputs (using org exporter's standard \texttt{EXCLUDE\_TAGS}):

\begin{description}
\item[{\emph{Slides only} material}] that won't be embedded in the handbook : surprise stuff for live audience, or HTML-only hacks;
\item[{\emph{Teachers only} material}] secret knowledge that only adults need
to know (for instance), which won't be exported;
\end{description}
\begin{description}
\item[{\emph{Handbook only} material}] stuff that only fits in the handbook, and/or only exports as \LaTeX{} and not HTML.
\end{description}

\begin{NOTES}
The choice to reveal or not some details to the students is quite arbitrary and depends on your pedagogical approach. I'm not advisable in this matter. YMMV.
\end{NOTES}

\subsection{Stuff only meant for presentation}
\label{sec:orgd5d7005}

Tagging a section/slide with \texttt{:slidesonly:} means it isn't exported in the handbooks.

Below is an example (or not)\ldots{}

\subsubsection{Regular slide (no tag on heading line)}
\label{sec:orgf702372}

There should be no "Only in the slides" after this section, in the
handbooks, as it has been tagged with \texttt{slidesonly}.

\subsection{Stuff only meant for teachers}
\label{sec:orgf45bf22}

Tagging a section/slide with \texttt{:teachersonly:} means it isn't exported in the students handbook (nor in the slides).

Below is an example\ldots{}

\subsubsection{Regular slide (no tag on heading line)}
\label{sec:org86ba111}

There should be no "Only for teachers" after this section, in the slides or in the
students handbook, as it has been tagged with \texttt{teachersonly}.

\subsubsection{Only for teachers}
\label{sec:orgd677ab4}

On the contrary this appears in the teachers handbook, as there's a \texttt{:teachersonly:} tag on the current head line.

\subsection{Notes only for the teachers}
\label{sec:orgb38e46b}

This slide/section contains notes, but only part of it is displayed in
the presentation notes included in the handbook. Special notes and are
kept only for the teachers handbook.

We use an org-mode drawer for that (additional benefit is that the content is folded by default in emacs, as it may be verbose and/or "sensitive") :
\begin{verbatim}
#+BEGIN_NOTES
This part of the note can be viewed by the students in the handbook.

:TEACHERSONLY:
Not this one
:END:
#+END_NOTES
\end{verbatim}

\begin{NOTES}


This part of the note can be viewed by the students in the handbook,
but not the rest.

\begin{description}
\item[{\textbf{TEACHERSONLY}}] but this part is only for the teachers.
\end{description}

You naughty ;-)
\end{NOTES}

\subsection{Stuff only in the handbooks}
\label{sec:orgef26dd5}

Just like sections are for slides only, others can be for the handbook
only, using the \texttt{handbookonly} tag. This may be useful for \textbf{Annex}
sections for instance, or for stuff that the HTML exporter won't like, with inline \LaTeX{}.

\subsection{Code colorization}
\label{sec:orga1a9bf5}
Code is colorized / highlighted in the slides :-)

\begin{NOTES}
Nice when like me, you're teaching Computer Science stuff

Depending of whether you export from Emacs (in X, with a particular
theme,\ldots{}) or with the Docker container, you'd get different results,
which depend on how htmlize is used, AFAIU.
\end{NOTES}

\subsection{Misc org-mode}
\label{sec:orgeded123}

\subsubsection{Babel powa}
\label{sec:orgf133c0e}
As you're using org-mode, its \texttt{babel} components are available, to embed source code in the same \texttt{lesson.org} source, and manage executable code and teaching material at once.

Look for \emph{literate programing} instructions in the \href{http://orgmode.org/manual/Working-with-source-code.html}{org-mode docs} to know more.

\subsubsection{Jumping to slide number}
\label{sec:orgf1d1201}

Included is the use of the
\href{https://github.com/SethosII/reveal.js-jump-plugin}{reveal.js jump
plugin} to allow jumping directly to slides \# by entering a number
and hitting RETURN. Quite handy while writing and testing slides.

\subsubsection{Fragmented SVG display}
\label{sec:org80c74e3}

The following SVG diagram is embedded in the HTML:
\begin{verbatim}
#+BEGIN_EXPORT html
  <svg
     width="210mm"
     height="297mm">
      <text
	 x="50"
	 y="50" class="fragment">A</text>
      <text
	 x="100"
	 y="50" class="fragment">B</text>
      <text
	 x="150"
	 y="50" class="fragment">C</text>
  </svg>
#+END_EXPORT

\end{verbatim}
Its elements with the \texttt{fragment} class can be displayed like ordinary
reveal.js fragments.


\subsection{Missing features ?}
\label{sec:orgd88323c}

Please try and talk to me to suggest new stuff and/or provide patches ;)

\begin{NOTES}
See the teacher's handbook for some ideas

\begin{itemize}
\item a way to manage graphics alongside the slides/handbook source\ldots{} not yet found a perfect solution, unless for plantuml with babel or likes (tikz\ldots{}).

\item some breadcrumb or recap feature / template to help give a sense of
the progression in the slides : only the progress bar isn't enough
and doesn't help giving the audience some kind of scaffolding to
hang on, for long presentations.
\end{itemize}
\end{NOTES}

\section{Authoring}
\label{sec:org60d7ef6}
\subsection{Modify only the lesson.org}
\label{sec:org4dc786c}

\textbf{Only one file should be edited for writing the lesson's material : \texttt{lesson.org}}

Only exception is modification of some configurations for title pages
and other bits that shouldn't change much in time (see section \hyperref[sec:org0c3c78e]{Configuration of layout}).

\subsection{Use Emacs org-mode exporters or the Docker container}
\label{sec:orgd99ea97}

You have 2 options to generate the different formats:
\begin{itemize}
\item either manualy use the standard org-mode exporters from inside Emacs
\item or use the Docker container for automation / reproducibility
\end{itemize}

\subsection{Manual export for final documents}
\label{sec:org42941c0}

We're using the standard exporters so each output format will be exported from its corresponding umbrella \texttt{.org} source.

Open the corresponding org-mode source and export :

\begin{description}
\item[{slides}] open \texttt{slides.org}, then \texttt{C-c C-e R ...} for \texttt{org-reveal} export (to \texttt{slides.html}), provided that you have loaded org-reveal in Emacs
\item[{handbook}] open \texttt{handbook.org}, then \texttt{C-c C-e l ...} for \LaTeX{} export (to \texttt{handbook.pdf})
\item[{teacher handbook}] open \texttt{teacher-handbook.org}, then \texttt{C-c C-e l ...} for \LaTeX{} export (to \texttt{teacher-handbook.pdf})
\end{description}

\begin{NOTES}
You're welcome to suggest improvements. But I'm not an Elisp hacker,
so I may not be able to maintain them. At the moment, the intent is to
rely on the original org-reveal only, as much as possible.
\end{NOTES}

\subsubsection{Exporting slides to HTML with org-reveal}
\label{sec:org7d94179}

Depending on how you installed org-reveal (\hyperref[sec:orgead5b59]{Git submodules} or otherwise), \texttt{org-reveal} may already be available.

If not yet, load it with \texttt{M-x load-file} from the location of its Git submodule (\texttt{elisp/org-reveal/ox-reveal.el} by default).

\begin{NOTES}
I'm not sure which solution is better : org-reveal from Git (hence the Git submodule) or from an Emacs package. Please report.
\end{NOTES}

\subsection{Use the docker container exporter}
\label{sec:orgf9c8d9e}

You may use the \texttt{olberger/docker-org-export} docker container image
I've prepared, to make org-mode exports. Or you may rebuild it
yourself (see below).

\subsubsection{Build the Docker container image}
\label{sec:org41f7c19}

This is recommended to avoid man in the middle, IMHO:

\begin{verbatim}
cd docker
docker build -t obergixlocal/docker-org-export .
\end{verbatim}

\subsubsection{Run the container}
\label{sec:org57c53ec}

Use the provided \texttt{docker/docker-org-export} script, which relies on
the \texttt{olberger/docker-org-export} container image. See how \url{Makefile} does it.

\subsection{Configuration of layout}
\label{sec:org0c3c78e}

Each \texttt{lesson.org} needs some configuration :
\begin{itemize}
\item Configure \texttt{org-reveal-title-slide} in \texttt{slides.org}.

\item Configure in the headers elements like:
\begin{itemize}
\item \emph{header} (\texttt{\textbackslash{}lhead\{...\}} and \texttt{\textbackslash{}rhead\{...\}})
\item and \emph{footer} (\texttt{\textbackslash{}lfoot\{...\}} and \texttt{\textbackslash{}rfoot\{...\}})
\end{itemize}

ex: \texttt{\#+LaTeX\_HEADER: \textbackslash{}rhead\{...\}} in \texttt{handbook.org} and
  \texttt{teacher-handbook.org}.
\end{itemize}

\begin{NOTES}
These may be better handled, but some limitations of the exporters or
my lack of knowledge/time have prevented a better result so
far. Improvements much welcome.
\end{NOTES}


\subsection{Printing slides}
\label{sec:org62c1db7}

I've tested \href{https://github.com/astefanutti/decktape}{DeckTape} using a Docker container containing \texttt{PhantomJS} and
\texttt{decktape} to convert the slides to a \href{slides.pdf}{single PDF document}.

See the provided \href{bin/decktape.sh}{decktape.sh} script that runs the container, bind-mounting the
working dir into the container, so that input and output files can be
found.

Note that I used a rebuilt Docker image, reusing the \href{https://raw.githubusercontent.com/astefanutti/decktape/master/Dockerfile}{DeckTape
Dockerfile}, rebuilding with something alongside:
\begin{verbatim}
docker build -t obergixlocal/decktape .
\end{verbatim}

\subsection{Known Issues}
\label{sec:orge14a105}

\subsubsection{Firefox issues ?}
\label{sec:org9852c05}

We have experienced issues with presentations made on some versions of Firefox, which are known by reveal.js maintainer\ldots{} maybe best viewed in chrome.

You may prefer to have a PDF variant of the slides (see \hyperref[sec:org62c1db7]{Printing slides}) in case.



\section{How it works / Installation}
\label{sec:org8b9b5f6}
\subsection{Use the source (Luke)}
\label{sec:org6d66808}

See the contents of the files\ldots{} but be wary that it's sometimes messy and incrementally obtained.

Emacs is your buddy.

Git clone from \texttt{https://gitlab.com/olberger/org-teaching.git} (see the \href{https://gitlab.com/olberger/org-teaching}{Gitlab project})

\subsubsection{Git submodules}
\label{sec:orgead5b59}

The repository contains Git submodules for :
\begin{itemize}
\item \texttt{reveal.js/}
\item \texttt{elisp/org-reveal}
\item reveal.js's jump plugin (\texttt{reveal.js-jump-plugin/})
\end{itemize}

So :
\begin{verbatim}
git submodule init
git submodule update
\end{verbatim}
You may prefer to install them another way (ELPA repo, CDN, etc.)

\begin{NOTES}
Refer to \href{https://github.com/yjwen/org-reveal/\#requirements-and-installation}{org-reveal's documentation} for more details.
\end{NOTES}

\subsection{Customize slides appearance}
\label{sec:org119c572}

\subsubsection{Reveal.js settings}
\label{sec:org18ef5e4}

See the org-reveal settings set in the sources and the docs for a detailed explanation.

I'm using the following for a "frugal" look close to what
powerpoint or beamer (?) could look like :

\begin{verbatim}
#+REVEAL_HLEVEL: 2
#+REVEAL_THEME: simple
#+REVEAL_TRANS: fade
#+REVEAL_SPEED: fast
#+REVEAL_MARGIN: 0.0
#+REVEAL_EXTRA_CSS: ./presentation.css
#+REVEAL_ROOT: ./reveal.js

#+OPTIONS: reveal_center:nil 
\end{verbatim}

\subsubsection{Section separators}
\label{sec:orge1f0584}

The highest level sections include the following properties below the heading line, to customize the look of the slide. 

\begin{verbatim}
:PROPERTIES:
:REVEAL_EXTRA_ATTR: class="center"
:reveal_background: #dbdbed
:END:
\end{verbatim}

This is intended to provide some visual sense of the transitions between sections. Please adapt and report.

\subsubsection{Title screen picture (logos, etc.)}
\label{sec:org490ee1d}

I'm not yet sure how much may be achieved with HTML and CSS for the
title page of the slides deck, so I've relied on the embedding of a
background image that will contain the logos and additional graphics. 

\begin{verbatim}
#+REVEAL_TITLE_SLIDE_BACKGROUND: ./media/title-slide-background.png
\end{verbatim}

I'm quite sure this could be improved.


\section{Annex}
\label{sec:org346fb8c}

\subsection{Thanks}
\label{sec:org3c66ecb}

\begin{itemize}
\item All contributors to org-mode (special kudos to Carsten Dominik and Bastien Guerry)
\item Yujie Wen for \texttt{org-reveal}
\item Hakim El Hattab for \texttt{reveal.js}
\item My colleagues at Telecom SudParis who had to teach with this tool without much rehersal
\item Our students who endured it for a brand new course (and included bugs)
\item Alexey Lebedeff for his
\href{https://github.com/binarin/docker-org-export}{docker-org-export}
Docker container
\end{itemize}

\subsection{Feedback}
\label{sec:orgc21112a}

I may be contacted from \href{http://www-public.tem-tsp.eu/\~berger\_o/\#sec-3}{my Web page} or via \href{https://gitlab.com/olberger/org-teaching}{the Gitlab project}.

\subsection{Usage reports}
\label{sec:org2d45199}

\subsubsection{2016-2018 at Telecom SudParis}
\label{sec:orge10fe15}

Created and used for 2 editions of teaching "Web Architecture and Applications" in the CSC4101 module at Telecom SudParis (Olivier Berger and colleagues)
\end{document}
